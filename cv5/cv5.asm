%include "rw32-2018.inc"

section .data
    ; zde budou vase data
    pole db 10,20,30,15,16,17,18,19,20,21,18
section .text
_main:
    push ebp
    mov ebp, esp
    
    ; zde bude vas kod
    ; spo��tat kolik nul je v EAX
    mov eax, 0
    
    mov ecx, 32
    xor ebx,ebx
    CYCLE:
        sal eax,1
        jc NOT_INC
        inc ebx
    NOT_INC:
        loop CYCLE
        
    ;ulozit 0xA1FF aplikovat masku 0x00ff
    ;horni polovina ebx do ax
    ;call writeuiint16
    mov ebx, 0xA1FF
    and ebx, 0x00FF
    
    shr ebx,16
    mov ax, bx
    
    call WriteUInt16
    
    ;zamente horn9 polovinu ebx s dolni polovinou eax
    mov eax, 11111111111111110000000000000000b
    mov ebx, 11111111111111110000000000000000b
    ror ebx, 16
    mov cx, bx
    mov bx, ax
    rol ebx, 16
    mov ax,cx

    ;eax 4x 8bit casti presunout a3,a2,a1,a0 -� a0,a2,a3,a1
    mov eax, 11111111000000001111111100000000b ; 
    rol eax, 16
    mov bl, al
    mov al, ah
    mov ah, bl ; a3 <-> a2
    rol eax, 8
    
    ;vypocet (-16/2 + -5*3)
    
    
    ; vypocet prumerne hodnoty v poli
    xor ax,ax
    mov ecx, 11
    plus:
        xor dx, dx
        mov dl, [pole+ecx*1-1]
        add ax, dx
        loop plus ; 204
    
    xor dx, dx
    mov bl, 11
    idiv bl ;204/11 = 18.54
    

    pop ebp
    ret