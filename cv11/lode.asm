%include "rw32-2018.inc"
 
extern _print_ships
extern @hit_check@8
extern _shot@8
extern _printf
extern _get_pointer_to_hits
 
section .data
;Informace o lodi jsou ulozeny v polich za sebou
;Kazda polozka obsahuje data v tomto poradi: x,y,sirka,vyska
;Kde x,y je souradnice horniho rohu
;v nize uvedenych datech jsou definovane 3 lode
ships_data dd   0,0,30,70,
           dd   220,200,50,20,
           dd   300,400,70,30
           dd   15,200,120,30
 
hit_str db 10,"******Zasah!******",10,0
nohit_str db 10,"******Vedle!******",10,0
 
area_w dd 500
area_h dd 500
 
 
count_hits dd 1
 
section .text
_main:
    push ebp
    mov ebp, esp
 
    ;UKOL c.1
    ;Nactete ze vstupu pomoci ReadUInt32 souradnici x a nasledne opet pomoci ReadUInt32 y
    ;Po nacteni vystrelu zavolejte funkci _shot@8, urcete o jakou konvenci se jedna a spravne ji pouzijte
    ;Funkce je deklarovana jako: void shot(int x, int y)
    ;Funkce provede zaznamenani pozice cile a bude si ho pamatovat pro dalsi casti prikladu
    ;Nactete ze stdin alespon 3 pozice kam maji dopadnout rakety
 
    mov ecx, 3
  nacti:
    call ReadUInt32
    ; v eax je X
    mov ebx, eax ; presun X
    call ReadUInt32
    ; v eax je Y
    
    ; zavolani _shot@8
    ; stdcall
    push eax
    push ebx
    call _shot@8
    
    loop nacti
 
 
    ;UKOL volitelny
    ;Zkuste pomoci funkce z jazyka C T_shot * get_pointer_to_shots(int * shots_count) ziskat hodnotu ukazatele
    ;a vypsat si pomoci debugeru pamet, kde jsou ulozene souradnice
 
 
 
    ;UKOL c.2
    ;Pomoci funkce @hit_check@8 zkontrolujte, zda doslo k zasahu,
    ;nasledne vypiste pomoci funkce printf text zacinajici na adresach hit_str nebo nohit_str
    ;Funkce je deklarovana jako: char hit_check(int *ships, int ship_count)
    ;int *ships - ukazatel pole poli s lodemi
    ;int ship_count - celkovy pocet lodi
    ;Pokud doslo k zasahu nejake lodi nejakou strelou, tak vrati 1, jinak 0
    
    ; fastcall
    mov ecx, ships_data
    mov edx, 3
    call @hit_check@8
 
    ; vracena hodnota snad v eax
    cmp eax, 0
    je not_hit
    
    ; call printf(hit_str)
    push hit_str
    call printf
    add esp, 4
    
    jmp if_end
  not_hit:
    ; call printf(nohit_str)
    push nohit_str
    call printf
    add esp, 4
    
  if_end:
 
    ;UKOL c.3
    ;Vykreslete hraci pole pomoci funkce _print_ships
    ;Funkce je deklarovana jako: void print_ships(int area_w, int area_h, int *ships, int ship_count)
 
    ; jazyk c
    push dword 3
    push ships_data
    push area_h
    push area_w4
    call _print_ships
    add esp, 16
 
    ;UKOL c.4 (Úkol na doma)
    ;Vypiste pozice x a y lodi do souboru isu.txt
 
 
    ;UKOL c.5  
    ;Udelejte kontrolu, zda raketa nemiri mimo hraci plochu promena area_w a area_h
 
    mov esp,ebp
    pop ebp
    ret