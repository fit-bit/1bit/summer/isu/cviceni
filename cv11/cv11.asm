%include "rw32-2018.inc"

extern printf

section .data
    ; zde budou vase data
    text db "Ahoj",0
    format db "%d",0

section .text
_main:
    push ebp
    mov ebp, esp
    
    ; zde bude vas kod
    push text
    ;call printf
    add esp,4
    
    ; printf("%d",20);
    mov eax, 20
    push eax
    push format
    call printf
    add esp, 8
    
    pop ebp
    ret