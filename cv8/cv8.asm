%include "rw32-2018.inc"

section .data
    ; zde budou vase data
    slovo resb 10
    retezec resb 50

section .text
replace:
    push ebp
    mov ebp, esp

    mov esp, ebp
    pop ebp
    ret ;pak

_main:
    push ebp
    mov ebp, esp
    
    mov ebx, 9
    mov edi, slovo
    call ReadString
    ; v eax pocet nactenych znaku
    push eax 
    
    mov ebx, 49
    mov edi, retezec
    call ReadString
    mov ebx, eax ; v ebx pocet znaku slova
    pop eax ;v eax pocet ynaku retezec
 
    mov ecx, eax
    sub ebx, eax
    cyklus_vnejsi:
        cmp ebx,0
        je cyklus_vnejsi_konec
        cyklus_vnitrni:
            cmp ecx,0
            jz cyklus_vnitrni_konec
            dec ebx
            
            ; telo vnitrniho cyklu
            mov dl, [retezec+ebx+ecx-1]
            cmp dl, [slovo+ebx+ecx-1]
            jne cyklus_vnitrni_konec ; pokud neni shoda, nemusime pokracovat v porovnani slova v danem miste
            
            dec ecx
            jmp cyklus_vnitrni  
        cyklus_vnitrni_konec:
        
        cmp ecx, -1
        jne ne_shoda; nenastala shoda
        
        mov ecx, eax
        cyklus_vnitrni_nahrazeni:
            cmp ecx,0
            jz cyklus_vnitrni_nahrazeni_konec
            dec ecx
            ; telo vnitrniho cyklu
            mov dl, 'X'
            mov [retezec+ebx+ecx], dl
               
            jmp cyklus_vnitrni_nahrazeni
        cyklus_vnitrni_nahrazeni_konec:
      
        ne_shoda:

        jmp cyklus_vnejsi
    cyklus_vnejsi_konec:
    pop ebp
    ret