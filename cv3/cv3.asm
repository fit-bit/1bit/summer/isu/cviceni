%include "rw32-2018.inc"

section .data
    ; zde budou vase data
    array_a dw 0,0xFFFF
    array_b db 0,0,0,1 
    
    array_1 dw 22,32,6,81,67,99
    array_2 dq 42,12,9,1,97,34
    
    pole1 dw 1,2,3,4,5,6,7,8,9,10
    pole2 dw 1,2,3,4,5,6,7,8,9,10
    
    a db 0,0,0,0
    b db 0,0,0,1
    c db 0,0,0,2
    d db 0,0,0,3
    p_array dd 0,0,0,0

section .text
_main:
    push ebp
    mov ebp, esp
    
    ; zde bude vas kod
    ;af
    mov al, 1111b
    mov bl, 1111b
 
    add al,bl
    
    ;zf
    xor al, al

    ;pf
    mov al, 1110b
    add al, 1
    
    ; Carry flag
    xor bx,bx
    mov ax,[array_a+1*2]
    mov bl,[array_b+3*1]
    add ax,bx
    
    ;nahradit v druhem poly hodnoty se sudym indexem hodnoty z prvniho pole
    xor eax,eax
    mov ax, [array_1+0*2]
    mov [array_2+0*4], eax
    
    xor eax,eax
    mov ax, [array_1+2*2]
    mov [array_2+2*4], eax
    
    xor eax,eax
    mov ax, [array_1+4*2]
    mov [array_2+4*4], eax
    

    ; pomoci add a sub vzgenerovat ZF,SF,CF,PF,AF
    ; ZF
    mov ax, 1
    sub ax, 1
    
    ;SF
    mov ax, 1
    sub ax, 6
    
    ;CF
    mov ax, 65535
    add ax, 1000
    
    ;PF
    mov ax, 10b
    add ax, 1b
 
    ; AF
    mov ax, 1111b
    add ax, 1b

    ; dve pole 10 prvek, vypocet pole1[i] = pole1[i] + pole2[i]-4. i=0..9 
    ;loop
    mov ecx, 10 ;povet iteraci
    cyklus: ;navesti
            xor ax, ax
            mov ax, [pole1+(ecx-1)*2]
            ;nebo displaicement
            ;mov ax, [pole1+(ecx-1)*2-2] 2 protoze word 16 bitu 
            add ax, [pole2+(ecx-1)*2]
            sub ax, 4
            mov [pole1+(ecx-1)*2], ax
           loop cyklus
    
    ;pole a,b,c,d. pole p_array ukayatele na pole a,b,c,d, vypsat posledni prvek c pomoci p_array
    mov eax, a
    mov [p_array+0*4], eax
    
    mov eax, b
    mov [p_array+1*4], eax
    
    mov eax, c
    mov [p_array+2*4], eax
    
    mov eax, d
    mov [p_array+3*4], eax
    
    mov ebx, [p_array+2*4]
    mov cl,  [ebx+3*1]
    
    pop ebp
    ret