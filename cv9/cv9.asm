%include "rw32-2018.inc"

section .data
    ; zde budou vase data
    string1 db "Ahoj"
    string2 db "xxxx"
    
    str1 dd "Ahoj"
    str2 dd "Ahoj"
    
    arg resb 11
    compare_str db "compare"
    replace_str db "replace12"
    arr1 db 10,20,30,40,50,60,1,8,9,10
    arr2 db 10,20,30,40,50,61,1,8,9,10

section .text
_main:
    push ebp
    mov ebp, esp
    
    ; zde bude vas kod
    ;mov edi, string1
    ;mov esi, string2
    
    ;movsb
    ;cmpsb
    
    ;mov al, 'o' m�lo by b�t x ale wtf
    ;scasb
    
    ;mov al, 'x'
    ;mov edi, string1
    
    ;mov ecx, 4 ; nahrazen� v�ech znak�
    ;rep stosb
    
    ;mov ecx, 4
    ;mov esi, str1
    ;mov edi, str2
    ;repz cmpsb
    
    ;1. na�te do arg �et�zec ze stdin
    mov ebx, 10
    mov edi, arg
    call ReadString
    
    ;2.
    ; if(strcmp(arg,"compare") == 0)
    mov ecx, eax
    mov esi, arg
    mov edi, compare_str
    repz cmpsb
    cmp ecx, 0
    jne if_compare_else
        ; ulo�en� na z�sobn�k z leva
        push arr1
        push arr2
        push dword 10
        call compare_array ; ukl�z� volan�
    if_compare_else:
    mov ecx, eax
    mov esi, arg
    mov edi, replace_str
    repz cmpsb
    cmp ecx, 0
    jne if_replace_else
        push arr1
        push arr2
        push dword 10
        call replace_a1_a2 ;ukl�z� volaj�c�
        add esp, 12 ;4*n ukazatele jsou 32b
    if_replace_else:
    
    pop ebp
    ret
    
    compare_array:
        push ebp
        mov ebp, esp
        ; arr1 = [ebp+16]
        ; arr2 = [ebp+12]
        ; count = [ebp+8]
        
        push edi
        push esi
        push ecx
        
        mov ecx, [ebp+8]
        mov edi, [ebp+12]
        mov esi, [ebp+16]
       cyklus:
        mov al, [edi+ecx*1-1]
        cmp [esi+ecx*1-1], al
        jnz nerovnaji
        
        loop cyklus
        mov eax, 1
        jmp rovnajise
        
        nerovnaji:
        mov eax, 0
        
        rovnajise:
        
        pop ecx
        pop esi
        pop edi
        mov esp, ebp
        pop ebp
        ret 12 ;4*n
        
    replace_a1_a2:
        push ebp
        mov ebp, esp
        
        
        mov esp, ebp
        pop ebp
        ret