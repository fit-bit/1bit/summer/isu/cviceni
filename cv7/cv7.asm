%include "rw32-2018.inc"

section .data
    ; zde budou vase data
    pole db 11,22,33,44

section .text
fun: ; vygeneruje stack overflow
    call funkce
    ret
    
funkce:
    push ebp
    mov ebp, esp
    sub esp, 12
    
    
    mov eax, [ebp+8] ; 30
    mov [ebp-4], eax
    add eax, [ebp+12] ; 20
    mov [ebp-8], eax
    add eax, [ebp+16] ; 10
    mov [ebp-12], eax
    
    mov esp, ebp
    pop ebp
    ret 12
    
; funkce s parametry char *pole, int pocet_prvku
; provede soucet prvku v poli, vysledek v eax
; prvni se preda pocet prvku
secti:
    push ebp
    mov ebp, esp
   
    xor eax, eax 
    mov ecx, [ebp+12]
    mov ebx, [ebp+8] 
    
    cyklus:
        add al, [ebx+ecx*1-1]
        loop cyklus
    
    mov esp, ebp
    pop ebp
    ret 8

_main:
    push ebp
    mov ebp, esp
        
    push dword 10
    push dword 20
    push dword 30
    call funkce
    ; zde bude vas kod

    ; secte pole
    push dword 4
    push dword pole
    call secti

    pop ebp
    ret