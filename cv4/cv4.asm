%include "rw32-2018.inc"

section .data
    ; zde budou vase data
    pole dw 10,20,8,1,256
    a db 5
    b db 7
    c dw 250
    d dw 1
    e dw 4
    
    v db 30

section .text
_main:
    push ebp
    mov ebp, esp
    
    ;xor ebx, ebx
    ;mov ecx, 5
    ;.cyklus:
     ;       add bx, [pole+ecx*2-2]
      ;      loop .cyklus
        
    ;v�po�et objemu lichob�n�ku v�sledek cx
    ;mov al,[a]
    ;mov bl,[c]
    ;add al, bl
    
    ;mul byte [v]
    
    ;xor dx, dx
    ;mov cx, 2
    ;div cx
    
    ;mov cx, ax
    
    ; x = 4*a+c/-10
    ;mov al,[a]
    ;mov cl, 4
    ;mul cl
    ;mov bx, ax
    
    ;xor ax, ax
    ;mov al, [c]
    ;mov cl, -10
    
    ;idiv cl
    
    ;add bx, ax nefunguje proto�e n�co v ah
    
    ;(a*b) 5*7 = 35
    mov al, [a]
    imul byte [b]
    mov bx, ax
    
    ;(d-c) = -249
    mov ax, [d]
    sub ax, [c]
    cwd
    
    ;(ax/e) -249/4 = -62.25
    mov cx, [e]
    idiv cx
    
    ;(+) 35 + -62.25 = -27
    add bx, ax
    
    ; zde bude vas kod
    

    pop ebp
    ret